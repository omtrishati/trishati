This project is build 'trishati build app'

### Instructions to run

1) download Trishati/build folder
2) go to trishati folder and install > npm install -g serve
3) run > serve -s build


### Download specific folder from project , for example let's download 'build' directory

1) mkdir test
2) cd test 
3) git init
4) git remote add -f origin <url>
5) git config core.sparseCheckout true
6) echo "build/" >> .git/info/sparse-checkout
7) git pull origin master