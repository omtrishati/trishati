import React from 'react';
// import './Contact.css';

function Contact() {
    const divStyle = {
        fontSize: "18px",

    };
    return (
        <div className='container p-2' style={divStyle}>
            <h3 className="slideRight">Contact Us</h3>
            <hr className="border-top my-3 slideRight" />

            <br />
            <p>

                <section className="text-center  mygradient slideLeft">
                    <article className="py-5">
                        <div>
                            <p>If you want best service call us now</p>
                            <h3 className="display-7">+91 8099334978</h3>
                            <p>Mail : omtrishati@hotmail.com</p>
                            {/* <button class="btn btn-warning">Contact Now</button> */}
                        </div>
                    </article>
                </section>

            </p>
            <br />
            <br />
            <hr className="border-top my-3 slideLeft" />
        </div>
    )
}

export default Contact;