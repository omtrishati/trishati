import React from 'react';

function Footer() {
    return (
        <footer className='fixed-bottom' >
            <p className="text-center bg-dark text-white mb-0">Copyright © Trishati 2019-{new Date().getFullYear()} .  All rights reserved</p>
        </footer>
    )
}

export default Footer;