import React from 'react';
// import './Services.css'

function Services() {
    const divStyle = {
        fontSize: "18px",

    };
    return (
        <div className='container p-2 ' style={divStyle}>
            <h3 className="slideRight">What do we offer</h3>
            <hr className="border-top my-3 slideRight" />
            <br />
            <br />
            <br />
            <p className="slideLeft">Trishati uniquely aims in providing you the stunning and most appealing website design!!! Our teams works carefully to craft every part of your the website to provide a matchless user-experience.We provide customized services from small to big companies as per their business needs. We not only make new websites, we also assist in innovating your older websites.We offer our services at a throw away price, with a promise of customer satisfaction!!!</p>

            <br />
            <br />
            <br />
            <br />
            <hr className="border-top my-3 slideLeft" />
        </div>
    )
}

export default Services;