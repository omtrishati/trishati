import React from 'react';

function ServiceCard(props) {
    // const divStyle = {
    //     backgroundColor: `${props.color}`,

    // };
    return (
        <div className="card p-auto m-3" style={{ boxShadow: "4px 4px 5px 4px purple" }} >
            <img className="card-img-top" src={props.icon} style={{ height: "100px", backgroundColor: `${props.color}` }} alt="Card" />
            {/* <hr className="border-top my-3" /> */}
            <div className="card-body bg-dark text-white">
                <h4 className="card-title" >{props.title}</h4>
                {/* <p className="card-text">Some example text.</p> */}
                {/* <a href="/" className="btn btn-primary">See Profile</a> */}
            </div>
        </div>
    )
}

export default ServiceCard;