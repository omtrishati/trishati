import React from 'react';
// import './About.css';

function About() {
    const divStyle = {
        fontSize: "18px",

    };
    return (
        <div className='container 0-2' style={divStyle}>
            <h3 className="slideRight">About Us</h3>
            <hr className="border-top my-3 slideRight" />
            <p className="slideLeft ">Our Mission
Aims to provide matchless Website Designs!!!
Trishati started on June  2019, headquartered in Bangalore. Our acute and innovative teams looks forward to create milestones in making pleasing designs, with clear headers, concise descriptions , links embellished with little graphics to display what the business emphasizes to every user who visits their sites, aiming to provide an unambiguous experience!!</p>
            <p className="slideLeft" style={divStyle}>Collaboration
            Looking for an exciting collaboration in web development?
            why should you collaborate with us? We ensure to build your knowledge in web development to unleash potential in building your experience.

            We are looking for active and energetic minds who are passionate in web development. Even if you don't hold the skill set we are ready to train you to explore your best!! If you are having the skill set, and looking for a platform to turn it into experience, we would love to!! And if you are experienced and bored of your old job, we are here to make your work life amazing.

We don't care much about your academics. But we do care about how passionate are you to work with us, your creativity, your commitment and your attitude to make our work environment wonderful!!!!</p>
            <br />
            <hr className="border-top my-3 slideLeft" />
        </div>

    )
}

export default About;