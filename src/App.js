import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from 'components/Header';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from 'components/Home';
import Services from 'components/Services';
import About from 'components/About';
import Contact from 'components/Contact';
import Footer from 'components/Footer';
import ServiceCard from 'components/ServiceCard';
import webDesign from 'images/web-design.svg'
import prodDev from 'images/prod-dev.svg'
// import webHosting from 'images/web-hosting.svg'
import cloudHost from 'images/cloud-host.svg'

function App() {
  return (
    <div className="App">
      {/* <header className="App-header bg-white">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <Router>
        <Header />
        <div className='d-flex flex-column' style={{ height: "80%" }}>
          <div className="flex-grow-1 row">
            <h1 class="text-center text-align-middle text-capitalize  col-lg-12 row" style={{ width: "100%", color: "black", background: "linear-gradient(to right bottom, #ffba82, white,green)" }}>
              <p className="p-2 col-lg-12 col-sm-12 headerSlideRight">Welcome to Om Trishati India Private Limited</p>
              <h5 className="col-lg-12 col-sm-12 headerSlideLeft">Finest brand heading towards excellence!!!</h5>
            </h1>
            <hr />
            <div className=" col-lg-12">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/services" component={Services} />
                <Route path="/about" component={About} />
                <Route path="/contact" component={Contact} />
              </Switch>
            </div>
          </div>
        </div>
        <div className='container ' style={{ background: "lineari-gradient(to right bottom, grey, darkgrey,black)" }}>
          <h3 className="text-dark"><u className=""><strong>Services Offered</strong></u></h3>
          {/* <hr /> */}
          <div class="row text-center mb-5">
            <div class="col-lg-4 ">
              <ServiceCard title='Web Designing' color='orange' icon={webDesign} />
            </div>
            <div class="col-lg-4 ">
              <ServiceCard title='Product Develpoment' color='green' icon={prodDev} />
            </div>
            <div class="col-lg-4 ">
              <ServiceCard title='Web Hosting' color='blue' icon={cloudHost} />
            </div>
          </div>


        </div>
        <Footer />
      </Router>
    </div >
  );
}

export default App;
